LOCAL_BIN:=$(CURDIR)/bin

run:
	go run cmd/lecture-6-demo/main.go

lint:
	golint ./...

test:
	go test -v ./...

.PHONY: build
build: vendor-proto .generate .build

PHONY: .generate
.generate:
		mkdir -p swagger
		mkdir -p pkg/lecture-6-demo
		protoc -I vendor.protogen \
				--go_out=pkg/lecture-6-demo --go_opt=paths=import \
				--go-grpc_out=pkg/lecture-6-demo --go-grpc_opt=paths=import \
				--grpc-gateway_out=pkg/lecture-6-demo \
				--grpc-gateway_opt=logtostderr=true \
				--grpc-gateway_opt=paths=import \
				--swagger_out=allow_merge=true,merge_file_name=api:swagger \
				api/lecture-6-demo/lecture-6-demo.proto
		mv pkg/lecture-6-demo/gitlab.com/siriusfreak/lecture-6-demo/pkg/lecture-6-demo/* pkg/lecture-6-demo/
		rm -rf pkg/lecture-6-demo/gitlab.com
		mkdir -p cmd/lecture-6-demo
		cd pkg/lecture-6-demo && ls go.mod || go mod init gitlab.com/siriusfreak/lecture-6-demo/pkg/lecture-6-demo && go mod tidy

.PHONY: generate
generate: .vendor-proto .generate

.PHONY: build
build:
		go build -o $(LOCAL_BIN)/lecture-6-demo cmd/lecture-6-demo/main.go

.PHONY: vendor-proto
vendor-proto: .vendor-proto

.PHONY: .vendor-proto
.vendor-proto:
		mkdir -p vendor.protogen
		mkdir -p vendor.protogen/api/lecture-6-demo
		cp api/lecture-6-demo/lecture-6-demo.proto vendor.protogen/api/lecture-6-demo/lecture-6-demo.proto
		@if [ ! -d vendor.protogen/google ]; then \
			git clone https://github.com/googleapis/googleapis vendor.protogen/googleapis &&\
			mkdir -p  vendor.protogen/google/ &&\
			mv vendor.protogen/googleapis/google/api vendor.protogen/google &&\
			rm -rf vendor.protogen/googleapis ;\
		fi


.PHONY: deps
deps: install-go-deps

.PHONY: install-go-deps
install-go-deps: .install-go-deps

.PHONY: .install-go-deps
.install-go-deps:
		ls go.mod || go mod init gitlab.com/siriusfreak/lecture-6-demo
		GOBIN=$(LOCAL_BIN) go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
		GOBIN=$(LOCAL_BIN) go get -u github.com/golang/protobuf/proto
		GOBIN=$(LOCAL_BIN) go get -u github.com/golang/protobuf/protoc-gen-go
		GOBIN=$(LOCAL_BIN) go get -u google.golang.org/grpc
		GOBIN=$(LOCAL_BIN) go get -u google.golang.org/grpc/cmd/protoc-gen-go-grpc
		GOBIN=$(LOCAL_BIN) go install google.golang.org/grpc/cmd/protoc-gen-go-grpc
		GOBIN=$(LOCAL_BIN) go install github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger


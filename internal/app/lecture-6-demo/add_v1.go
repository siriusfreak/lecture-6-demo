package lecture_6_demo

import (
	"context"

	desc "gitlab.com/siriusfreak/lecture-6-demo/pkg/lecture-6-demo"
	"google.golang.org/protobuf/types/known/emptypb"
)

func (a *Lecture6DemoAPI)AddV1(ctx context.Context, req *desc.AddRequestV1) (*emptypb.Empty, error) {
	return &emptypb.Empty{}, nil
}

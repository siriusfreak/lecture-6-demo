module gitlab.com/siriusfreak/lecture-6-demo

go 1.16

require (
	github.com/golang/glog v0.0.0-20210429001901-424d2337a529 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/grpc-ecosystem/grpc-gateway v1.16.0 // indirect
	gitlab.com/siriusfreak/lecture-6-demo/pkg/lecture-6-demo v0.0.0-00010101000000-000000000000 // indirect
	golang.org/x/net v0.0.0-20210805182204-aaa1db679c0d // indirect
	golang.org/x/sys v0.0.0-20210809222454-d867a43fc93e // indirect
	google.golang.org/genproto v0.0.0-20210809142519-0135a39c2737 // indirect
	google.golang.org/grpc v1.39.1 // indirect
	google.golang.org/grpc/cmd/protoc-gen-go-grpc v1.1.0 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)

replace gitlab.com/siriusfreak/lecture-6-demo/pkg/lecture-6-demo => ./pkg/lecture-6-demo
